# Terminal Themes

A collection of [iTerm](https://iterm2.com), [Blink](https://www.blink.sh) & **Terminal** themes either created or collected. Also included is a [website](https://lawrencechin.gitlab.io/Terminal-Themes) for playing around with colour schemes.

![terminal themes](image.jpg)
