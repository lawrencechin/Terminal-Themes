# Previously this was added to zshenv and used in shell scripts
# But that isn't conducive to running those scripts in other shells
# I leave this here as a reference for when you undoubtedly forget
# how to write these escape codes

########## COLOURS! ###########
# reset
export nc="\033[0m"       # text reset

# regular colors
export black="\033[0;30m"        # black
export red="\033[0;31m"          # red
export green="\033[0;32m"        # green
export yellow="\033[0;33m"       # yellow
export blue="\033[0;34m"         # blue
export magenta="\033[0;35m"       # magenta
export cyan="\033[0;36m"         # cyan
export white="\033[0;37m"        # white

# bold
export bold="\033[1m"            # bold regular
export bblack="\033[1;30m"       # black
export bred="\033[1;31m"         # red
export bgreen="\033[1;32m"       # green
export byellow="\033[1;33m"      # yellow
export bblue="\033[1;34m"        # blue
export bmagenta="\033[1;35m"      # magenta
export bcyan="\033[1;36m"        # cyan
export bwhite="\033[1;37m"       # white

# underline
export underline="\033[4m"       # underline regular
export ublack="\033[4;30m"       # black
export ured="\033[4;31m"         # red
export ugreen="\033[4;32m"       # green
export uyellow="\033[4;33m"      # yellow
export ublue="\033[4;34m"        # blue
export umagenta="\033[4;35m"      # magenta
export ucyan="\033[4;36m"        # cyan
export uwhite="\033[4;37m"       # white

# background
export on_black="\033[40m"       # black
export on_red="\033[41m"         # red
export on_green="\033[42m"       # green
export on_yellow="\033[43m"      # yellow
export on_blue="\033[44m"        # blue
export on_magenta="\033[45m"      # magenta
export on_cyan="\033[46m"        # cyan
export on_white="\033[47m"       # white

########## COLOURS! ###########
