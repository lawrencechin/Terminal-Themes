foreground_color = "#000000";
background_color = "#feffff";
bold = "#688088";
cursor_color = "#688088";
black = "#000000";
red = "#eb4b3f";
green = "#00990f";
yellow = "#dc6122";
blue = "#603da8";
magenta = "#e02ea9";
cyan = "#009cd9";
white = "#a2b0b0";
lightBlack = "#688088";
lightRed = "#eb4b3f";
lightGreen = "#00990f";
lightYellow = "#dc6122";
lightBlue = "#7c86ce"; 
lightMagenta = "#ff6ca1"; 
lightCyan = "#009cd9";
lightWhite = "#a2b0b0";

t.prefs_.set("color-palette-overrides", [ 
    black, red, green, yellow,
    blue, magenta, cyan, white,
    lightBlack, lightRed, lightGreen, lightYellow,
    lightBlue, lightMagenta, lightCyan, lightWhite
]);
t.prefs_.set( "cursor-blink", true );
t.prefs_.set( "cursor-color", cursor_color );
t.prefs_.set( "foreground-color", foreground_color );
t.prefs_.set( "background-color", background_color );
