foreground_color = "#e0fbfe";
background_color = "#2a1e32";
bold = "#fff9e4";
cursor_color = "#ee62ff";
black = "#000304";
red = "#e35997";
green = "#2dbc63";
yellow = "#ffd782";
blue = "#9b59e3";
magenta = "#6171a5";
cyan = "#00abde";
white = "#fff9e4";
lightBlack = "#93a1a1";
lightRed = "#e383ba";
lightGreen = "#2dbc63";
lightYellow = "#efca78";
lightBlue = "#b07be9";
lightMagenta = "#7c87a5";
lightCyan = "#54bfde";
lightWhite = "#fff9e4";

t.prefs_.set("color-palette-overrides", [ 
    black, red, green, yellow,
    blue, magenta, cyan, white,
    lightBlack, lightRed, lightGreen, lightYellow,
    lightBlue, lightMagenta, lightCyan, lightWhite
]);
t.prefs_.set( "cursor-blink", true );
t.prefs_.set( "cursor-color", cursor_color );
t.prefs_.set( "foreground-color", foreground_color );
t.prefs_.set( "background-color", background_color );
