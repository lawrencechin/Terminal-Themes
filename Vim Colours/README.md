# Vim Colours

The main aim is to return from a **lua** based theme system that requires *termguicolors* to a **vim** theme that acts as a passthrough for terminal colours. This was previously achieved using a modified version of the builtin theme *Delek*. The plan is to take the new default **neovim** theme in conjunction with the **lua** themes to update *Delek* once more to include important *hi groups* that weren't present in the previous version of *Delek*. In order to do this, we have to remove all *gui* definitions and only use *cterm*. We need to translate the **lua** scheme to the 0-15 terminal colours and ensure they work for a variety of themes, both dark and light.

## Files and Directories

- Arsenix lua Scheme: the lua file containing highlight groups and corresponding colours
- Default nvim Scheme: the new default theme for **neovim** used a starting point for essential highlight groups and used for its structure.
- Default_Delek: restructured *Delek* based on default theme.
- hi_groups: all the groups from Arsenixc lua scheme, default scheme and delek_extended.
- delek_expandido.vim: the actual scheme
- generate_vim_samples.py: The *python* script uses the text file to generate a html page of all the *xterm* colours that one can use in vim. The text file has all the named colours with the corresponding cterm code and hex code. 

