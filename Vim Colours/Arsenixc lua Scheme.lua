local scheme = require("arsenixc.colours")
local setup = {}
local g = vim.g

---Color table for the editor
--@return syntax table: table with the groups and its respective colors
function setup.load_editor()

    ---------------------
    -- EDITOR COLORS --
    ---------------------

    local syntax = {
        Boolean = { fg = scheme.Orange }, -- a boolean constant: TRUE, false
        Character =	{ fg = scheme.Orange }, -- any character constant: 'c', '\n'
        ColorColumn = { fg = scheme.none, bg = scheme.Grey }, --used for the columns set with 'colorcolumn'
        CommandMode =	{ fg = scheme.Text_Colour }, -- Command mode message in the cmdline
        Comment = { fg = scheme.Grey },
        Conceal = { fg = scheme.Blue }, -- placeholder characters substituted for concealed text (see 'conceallevel')
        Conditional = { fg = scheme.Orange, bold = true },
        Constant =	{ fg = scheme.Blue }, -- any constant
        Cursor = { fg = scheme.Strong_Purple, bg = scheme.Strong_Purple }, -- the character under the cursor
        CursorColumn =	{ fg = scheme.none, bg = scheme.Grey }, -- Screen-column at the cursor, when 'cursorcolumn' is set.
        CursorIM = {}, -- like Cursor, but used when in IME mode
        CursorLine = { fg = scheme.none, bg = scheme.Grey }, -- Screen-line at the cursor, when 'cursorline' is set.Low-priority if foreground (ctermfg OR guifg) is not set.
        CursorLineNr =	{ bg = scheme.Grey }, -- Like LineNr when 'cursorline' or 'relativenumber' is set for the cursor line.
        DashboardCenter = { fg = scheme.Text_Colour },
        DashboardFooter = { fg = scheme.Green, italic = true },
        DashboardHeader = { fg = scheme.Orange },
        DashboardShortCut = { fg = scheme.Red },
        Debug = { fg = scheme.Red, bg = scheme.none, sp = scheme.none }, -- debugging statements
        Define = { fg = scheme.Purple }, -- preprocessor #define
        Delimiter =	{ fg = scheme.Green }, -- character that needs attention like , or .
        DiffAdd = { fg = scheme.Green, bg = scheme.none }, -- diff mode: Added line
        DiffChange = { fg = scheme.Orange, bg = scheme.none }, --diff mode: Changed line
        DiffDelete = { fg = scheme.Red, bg = scheme.none }, -- diff mode: Deleted line
        DiffText = { fg = scheme.Bold_Italic, bold = true }, -- diff mode: Changed text within a changed line
        Directory = { fg = scheme.Purple }, -- directory names (and other special names in listings)
        EndOfBuffer = { fg = scheme.Grey },
        Error = { fg = scheme.Red, bold = true }, -- any erroneous construct
        ErrorMsg = { bold = true }, -- error messages
        Exception =	{ fg = scheme.Red }, -- try, catch, throw
        Float = { fg = scheme.Blue }, -- a floating point constant: 2.3e10
        FloatBorder = { fg = scheme.Slate_Grey, bg = scheme.none, sp = scheme.none },
        FoldColumn = { fg = scheme.Slate_Grey }, -- 'foldcolumn'
        Folded = { fg = scheme.Slate_Grey }, -- line used for closed folds
        Function = { fg = scheme.Magenta },
        Identifier = { fg = scheme.Blue },
        Ignore = { fg = scheme.none }, -- left blank, hidden
        IncSearch = { fg = scheme.none, bg = scheme.none, reverse = true }, -- 'incsearch' highlighting; also used for the text replaced with ":s///c"
        Include = { fg = scheme.Purple }, -- preprocessor #include
        InsertMode = { fg = scheme.Green }, -- Insert mode message in the cmdline
        Keyword = { fg = scheme.Purple, bold = true },
        Label = { fg = scheme.Yellow }, -- case, default, etc.
        LineNr = { fg = scheme.Grey }, -- Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
        Macro = { fg = scheme.Blue }, -- same as Define
        MatchParen = { sp = scheme.Pink, underline = true }, -- The character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
        ModeMsg = {}, -- 'showmode' message (e.g., "-- INSERT -- ")
        MoreMsg = { fg = scheme.Strong_Purple }, -- |more-prompt|
        MsgArea = { fg = scheme.Text_Colour, bg = scheme.none },
        NonText = { fg = scheme.Pink }, -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
        NormalFloat = { fg = scheme.Text_Colour }, -- normal text and background color for floating windows
        NormalMode = { fg = scheme.Text_Colour }, -- Normal mode message in the cmdline
        Number = { fg = scheme.Orange }, -- a number constant: 5
        Operator =	{ fg = scheme.Slate_Grey }, -- sizeof", "+", "*", etc.
        Pmenu =	{ fg = scheme.Text_Colour, bg = scheme.Popup_Colour }, -- Popup menu: normal item.
        PmenuSbar = { bg = scheme.Slate_Grey }, -- Popup menu: scrollbar.
        PmenuSel = { fg = scheme.Yellow, reverse = true, bold = true }, -- Popup menu: selected item.
        PmenuThumb = { bg = scheme.Grey }, -- Popup menu: Thumb of the scrollbar.
        PreCondit =	{ fg = scheme.Yellow }, -- preprocessor #if, #else, #endif, etc.
        PreProc =	{ fg = scheme.Magenta }, -- generic Preprocessor
        Question = { fg = scheme.Green, bold = true }, -- |hit-enter| prompt and yes/no questions
        QuickFixLine = {}, -- Current |quickfix| item in the quickfix window. Combined with |hl-CursorLine| when the cursor is there.
        Repeat = { fg = scheme.Purple, bg = scheme.none, sp = scheme.none },
        ReplacelMode =	{}, -- Replace mode message in the cmdline
        Search = { fg = scheme.Green, bold = true, underline = true }, -- Last search pattern highlighting (see 'hlsearch').Also used for similar items that need to stand out.
        SignColumn = { fg = scheme.none, bg = scheme.none, sp = scheme.none },
        Special = { fg = scheme.Orange }, -- any special symbol
        SpecialChar = { fg = scheme.Blue }, -- special character in a constant
        SpecialComment = { fg = scheme.Grey }, -- special things inside a comment
        SpecialKey = { fg = scheme.Purple }, -- Unprintable characters: text displayed differently from what it really is.But not 'listchars' whitespace. |hl-Whitespace|
        SpellBad = { sp = scheme.Red, undercurl = true }, -- Word that is not recognized by the spellchecker. |spell| Combined with the highlighting used otherwise.
        SpellCap = { sp = scheme.Blue, undercurl = true }, -- Word that should start with a capital. |spell| Combined with the highlighting used otherwise.
        SpellLocal = { sp = scheme.Purple, undercurl = true }, -- Word that is recognized by the spellchecker as one that is used in another region. |spell| Combined with the highlighting used otherwise.
        SpellRare = { sp = scheme.Yellow, undercurl = true }, -- Word that is recognized by the spellchecker as one that is hardly ever used.|spell| Combined with the highlighting used otherwise.
        Statement =	{ fg = scheme.Purple, bold = true }, -- any statement
        StatusLine = { fg = scheme.Magenta, underline = true }, -- status line of current window
        StatusLineNC = { fg = scheme.Grey, underline = true }, -- status lines of not-current windows Note: if this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
        StatusLineTerm = { fg = scheme.Purple, underline = true }, -- status line of current terminal window
        StatusLineTermNC = { link = "StatusLineNC" }, -- status lines of not-current terminal windows Note: if this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
        StorageClass = { fg = scheme.Bold_Italic }, -- static, register, volatile, etc.
        String = { fg = scheme.Green, bg = scheme.none },
        Structure =	{ fg = scheme.Purple }, -- struct, union, enum, etc.
        TabLine = { fg = scheme.Strong_Purple, bg = scheme.none, underline = true },
        TabLineFill = { fg = scheme.Strong_Purple, bg = scheme.none, underline = true }, -- tab pages line, where there are no labels
        TabLineSel = { fg = scheme.Strong_Purple, bold = true, reverse = true }, -- tab pages line, active tab page label
        Tag = { fg = scheme.Purple }, -- you can use CTRL-] on this
        Title = { fg = scheme.Magenta, bold = true }, -- titles for output from ":set all", ":autocmd" etc.
        Todo = { fg = scheme.Yellow, bold = true, italic = true }, -- anything that needs extra attention; mostly the keywords TODO FIXME and XXX
        Type = { fg = scheme.Purple }, -- int, long, char, etc.
        Typedef = { fg = scheme.Blue }, -- A typedef
        Underlined = { fg = scheme.Blue, underline = true }, -- text that stands out, HTML links
        VertSplit = { fg = scheme.Slate_Grey, bg = scheme.none, sp = scheme.none },
        Visual = { fg = scheme.none, bg = scheme.none, reverse = true }, -- Visual mode selection
        VisualMode = { fg = scheme.Text_Colour }, -- Visual mode message in the cmdline
        VisualNOS = { fg = scheme.none, bg = scheme.none, reverse = true }, -- Visual mode selection when vim is "Not Owning the Selection".
        WarningMsg = { fg = scheme.Orange }, -- warning messages
        Warnings = { fg = scheme.Yellow, bg = scheme.none, reverse = true },
        Whitespace =	{ fg = scheme.Text_Colour }, -- "nbsp", "space", "tab" and "trail" in 'listchars'
        WildMenu = { fg = scheme.Text_Colour }, -- current match in 'wildmenu' completion
        healthError = { fg = scheme.Red },
        healthSuccess = { fg = scheme.Green },
        healthWarning = { fg = scheme.Orange },
        lCursor = { fg = scheme.Blue, bg = scheme.Blue, sp = scheme.none },
        qfLineNr = { fg = scheme.Yellow, bg = scheme.none, sp = scheme.none }, -- Line numbers for quickfix lists
    }

    if g.arsenix_transparent_background then
        syntax.Normal = { fg = scheme.Text_Colour }
    else
        syntax.Normal = { fg = scheme.Text_Colour, bg = scheme.Background_Colour }
    end

    return syntax
end

function setup.load_plugins()
    ---------------------
    --  PLUGIN COLORS  --
    ---------------------
    local syntax = {
        -----------------------
        -- TREESITTER COLORS --
        -----------------------

        [ "@annotation" ] = { link = "Comment" }, -- For C++/Dart attributes, annotations that can be attached to the code to denote some kind of meta information.
        [ "@attribute" ] = { fg = scheme.Yellow }, -- (unstable) TODO: docs
        [ "@boolean" ] = { link = "Boolean" }, -- For booleans.
        [ "@character" ] = { link = "Character" }, -- For characters.
        [ "@comment" ] = { link = "Comment" }, -- For comment blocks.
        [ "@conditional" ] = { link = "Conditional" }, -- For keywords related to conditionnals.
        [ "@constant" ] = { link = "Constant" }, -- For constants
        [ "@constant.builtin" ] = { link = "Constant"}, -- For constant that are built in the language: `nil` in Lua.
        [ "@constant.macro" ] = { link = "Constant" }, -- For constants that are defined by macros: `NULL` in C.
        [ "@constructor" ] = { link = "PreProc" }, -- For constructor calls and definitions: `= { }` in Lua, and Java constructors.
        [ "@error" ] = { link = "Error" }, -- For syntax/parser errors.
        [ "@exception" ] = { link = "Exception" }, -- For exception related keywords.
        [ "@field" ] = { fg = scheme.Text_Colour }, -- For fields.
        [ "@float" ] = { link = "Float" }, -- For floats.
        [ "@function" ] = { fg = scheme.Blue }, -- For function (calls and definitions).
        [ "@function.builtin" ] = { link = "Function"}, -- For builtin functions: `table.insert` in Lua.
        [ "@function.macro" ] = { fg = scheme.Bold_Italic }, -- For macro defined fuctions (calls and definitions): each `macro_rules` in Rust.
        [ "@include" ] = { link = "Include" }, -- For includes: `#include` in C, `use` or `extern crate` in Rust, or `require` in Lua.
        [ "@keyword" ] = { link = "Keyword" }, -- For keywords that don't fall in previous categories.
        [ "@keyword.function" ] = { link = "Function" }, -- For keywords used to define a function.
        [ "@label" ] = { link = "Label" }, -- For labels: `label:` in C and `:label:` in Lua.
        [ "@literal" ] = { fg = scheme.Text_Colour }, -- Literal text.
        [ "@lsp.type.comment" ] = { link = "@comment" },
        [ "@lsp.type.enum" ] = { link = "@type" },
        [ "@lsp.type.interface" ] = { link = "Identifier" },
        [ "@lsp.type.keyword" ] = { link = "@keyword" },
        [ "@lsp.type.namespace" ] = { link = "@namespace" },
        [ "@lsp.type.parameter" ] = { link = "@parameter" },
        [ "@lsp.type.property" ] = { link = "@property" },
        [ "@lsp.type.variable" ] = {}, -- use treesitter styles for regular variables
        [ "@lsp.typemod.function.defaultLibrary" ] = { link = "@function.builtin" },
        [ "@lsp.typemod.method.defaultLibrary" ] = { link = "@function.builtin" },
        [ "@lsp.typemod.operator.injected" ] = { link = "@operator" },
        [ "@lsp.typemod.string.injected" ] = { link = "@string" },
        [ "@lsp.typemod.variable.defaultLibrary" ] = { link = "@variable.builtin" },
        [ "@lsp.typemod.variable.injected" ] = { link = "@variable" },
        [ "@method" ] = { fg = scheme.Blue }, -- For method calls and definitions.
        [ "@namespace" ] = { fg = scheme.Yellow }, -- For identifiers referring to modules and namespaces.
        [ "@number" ] = { link = "Number"}, -- For all numbers
        [ "@operator" ] = { link = "Operator" }, -- For any operator: `+`, but also `->` and `*` in C.
        [ "@parameter" ] = { fg = scheme.Slate_Grey }, -- For parameters of a function.
        [ "@parameter.reference" ]= { fg = scheme.Slate_Grey }, -- For references to parameters of a function.
        [ "@property" ] = { fg = scheme.Text_Colour }, -- Same as `TSField`,accesing for struct members in C.
        [ "@punctuation.bracket" ] = { link = "PreProc" }, -- For brackets and parens.
        [ "@punctuation.delimiter" ] = { fg = scheme.Slate_Grey }, -- For delimiters ie: `.`
        [ "@punctuation.special" ] = { fg = scheme.Slate_Grey }, -- For special punctutation that does not fall in the catagories before.
        [ "@repeat" ] = { link = "Repeat" }, -- For keywords related to loops.
        [ "@string" ] = { fg = scheme.Green }, -- For strings.
        [ "@string.escape" ] = { fg = scheme.Slate_Grey }, -- For escape characters within a string.
        [ "@string.regex" ] = { fg = scheme.Blue }, -- For regexes.
        [ "@structure" ] = { link = "Structure" },
        [ "@symbol" ] = { fg = scheme.Yellow }, -- For identifiers referring to symbols or atoms.
        [ "@tag" ] = { link = "Tag" }, -- Tags like html tag names.
        [ "@tag.delimiter" ] = { fg = scheme.Slate_Grey }, -- Tag delimiter like `<` `>` `/`
        [ "@text" ] = { fg = scheme.Text_Colour }, -- For strings considered text in a markup language.
        [ "@text.emphasis" ] = { fg = scheme.Bold_Italic, italic = true }, -- For text to be represented with emphasis.
        [ "@text.reference" ] = { fg = scheme.Bold_Italic }, -- Currently used in MD for link text
        [ "@text.strike" ] = { sp = scheme.Red, strikethrough = true }, -- For strikethrough text.
        [ "@text.strong" ] = { fg = scheme.Bold_Italic, bold = true },
        [ "@text.underline" ] = { fg = scheme.Bold_Italic, underline = true }, -- For text to be represented with an underline.
        [ "@title" ] = { fg = scheme.Magenta, bold = true }, -- Text that is part of a title.
        [ "@type" ] = { link = "Type" }, -- For types.
        [ "@type.builtin" ] = { fg = scheme.Yellow }, -- For builtin types.
        [ "@uRI" ] = { fg = scheme.Blue }, -- Any URI like a link or email.
        [ "@variable" ] = { fg = scheme.Text_Colour }, -- Any variable name that does not have another highlight.
        [ "@variable.builtin" ] = { fg = scheme.Text_Colour }, -- Variable names that are defined by the languages, like `this` or `self`.

        ----------------
        -- LSP COLORS --
        ----------------

        DiagnosticError = { fg = scheme.Red }, -- used for "Error" diagnostic virtual text
        DiagnosticHint = { fg = scheme.Orange },  -- used for "Hint" diagnostic virtual text
        DiagnosticInfo = { fg = scheme.Mauve }, -- used for "Information" diagnostic virtual text
        DiagnosticWarn = { fg = scheme.Yellow }, -- used for "Warning" diagnostic signs in sign column
        DiagnosticFloatingError = { fg = scheme.Red }, -- used for "Error" diagnostic messages in the diagnostics float
        DiagnosticFloatingHint = { fg = scheme.Orange  }, -- used for "Hint" diagnostic messages in the diagnostics float
        DiagnosticFloatingInfo = { fg = scheme.Mauve }, -- used for "Information" diagnostic messages in the diagnostics float
        DiagnosticFloatingWarn = { fg = scheme.Yellow }, -- used for "Warning" diagnostic messages in the diagnostics float
        DiagnosticSignError = { fg = scheme.Magenta }, -- used for "Error" diagnostic signs in sign column
        DiagnosticSignHint = { fg = scheme.Orange }, -- used for "Hint" diagnostic signs in sign column
        DiagnosticSignInfo = { fg = scheme.Purple },  -- used for "Information" diagnostic signs in sign column
        DiagnosticSignWarn = { fg = scheme.Yellow }, -- used for "Warning" diagnostic signs in sign column
        DiagnosticUnderlineError = { undercurl = true, sp = scheme.Red }, -- used to underline "Error" diagnostics.
        DiagnosticUnderlineHint = { undercurl = true, sp = scheme.Orange }, -- used to underline "Hint" diagnostics.
        DiagnosticUnderlineInfo = { undercurl = true, sp = scheme.Mauve }, -- used to underline "Information" diagnostics.
        DiagnosticUnderlineWarn = { undercurl = true, sp = scheme.Yellow }, -- used to underline "Warning" diagnostics.
        DiagnosticVirtualTextError = { fg = scheme.Red }, -- Virtual text "Error"
        DiagnosticVirtualTextHint = { fg = scheme.Orange  }, -- Virtual text "Hint"
        DiagnosticVirtualTextInfo = { fg = scheme.Mauve }, -- Virtual text "Information"
        DiagnosticVirtualTextWarn = { fg = scheme.Yellow }, -- Virtual text "Warning"
        LspReferenceRead = { fg = scheme.Text_Colour }, -- used for highlighting "read" references
        LspReferenceRead = { fg = scheme.Text_Colour },
        LspReferenceText = { fg = scheme.Text_Colour }, -- used for highlighting "text" references
        LspReferenceText = { fg = scheme.Text_Colour },
        LspReferenceWrite = { fg = scheme.Text_Colour }, -- used for highlighting "write" references

        ----------------
        -- NVIM CMP --
        ----------------

        CmpItemMenuDefault = { link = "NormalFloat" },

        ---------------
        -- TELESCOPE --
        ---------------

        TelescopeBorder = { fg = scheme.Slate_Grey },
        TelescopeMatching = { fg = scheme.Magenta },
        TelescopeMultiSelection = {},
        TelescopeNormal = {},
        TelescopePreviewBorder = { fg = scheme.Slate_Grey },
        TelescopePromptBorder = { fg = scheme.Slate_Grey },
        TelescopePromptPrefix = { fg = scheme.Strong_Purple },
        TelescopeResultsBorder = { fg = scheme.Slate_Grey },
        TelescopeSelection = { reverse = true },
        TelescopeSelectionCaret = { fg = scheme.Strong_Purple },
        TelescopePrompt = { fg = scheme.Magenta },
    }
    return syntax
end

function setup.load_langs()

    ---------------------
    -- LANGUAGE COLORS --
    ---------------------

    local syntax = {

        --------------
        -- MARKDOWN --
        --------------

        markdownBlockquote = { fg = scheme.Pink },
        markdownBold = { fg = scheme.Bold_Italic, bold = true },
        markdownBoldItalic = { fg = scheme.Bold_Italic, bold = true, italic = true },
        markdownCode = { fg = scheme.Slate_Grey },
        markdownCodeBlock = { fg = scheme.Slate_Grey },
        markdownCodeDelimiter = { fg = scheme.Grey },
        markdownH1 = { fg = scheme.Strong_Purple, bold = true },
        markdownH2 = { fg = scheme.Purple, bold = true },
        markdownH3 = { fg = scheme.Purple, bold = true },
        markdownH4 = { fg = scheme.Purple, bold = true },
        markdownH5 = { fg = scheme.Purple, bold = true },
        markdownH6 = { fg = scheme.Purple, bold = true },
        markdownHeadingDelimiter = { fg = scheme.Magenta },
        markdownHeadingRule = { fg = scheme.Purple, bold = true },
        markdownId = { fg = scheme.Strong_Purple },
        markdownIdDeclaration = { fg = scheme.Green },
        markdownItalic = { fg = scheme.Bold_Italic, italic = true },
        markdownLinkDelimiter = { fg = scheme.Pink },
        markdownLinkTextDelimiter = { fg = scheme.Magenta },
        markdownLinkText = { fg = scheme.Bold_Italic },
        markdownListMarker = { fg = scheme.Red },
        markdownOrderedListMarker = { fg = scheme.Magenta },
        markdownRule = { fg = scheme.Grey },
        markdownUrl = { fg = scheme.Slate_Grey },
        markdownUrlDelimiter = { fg = scheme.Magenta },
        markdownUrlTitleDelimiter = { fg = scheme.Green },

        ----------
        -- HTML --
        ----------

        --[[
        MatchTag = { "underline" },
        htmlArg = { fg = scheme.Purple },
        htmlEndTag = { fg = scheme.Grey },
        htmlLink = { fg = scheme.Orange },
        htmlSpecialTagName = { fg = scheme.Strong_Purple },
        htmlTag = { fg = scheme.Grey },
        htmlTagN = { fg = scheme.Strong_Purple },
        htmlTagName = { fg = scheme.Strong_Purple },
        --]]

        ---------
        -- CSS --
        ---------
        
        --[[
        cssFontAttr = { fg = scheme.Orange },
        cssAttrComma = { fg = scheme.Purple },
        cssIdentifier = { fg = scheme.Strong_Purple },
        cssImportant = { fg = scheme.Red },
        cssInclude = { fg = scheme.Green },
        cssIncludeKeyword = { fg = scheme.Purple },
        cssMediaType = {},
        cssProp = { fg = scheme.Slate_Grey },
        cssAttributeSelector = { fg = scheme.Green },
        cssBraces = { fg = scheme.Slate_Grey },
        cssClassName = { fg = scheme.Purple },
        cssClassNameDot = { fg = scheme.Purple },
        cssDefinition = {},
        cssFontDescriptor = { fg = scheme.Purple },
        cssFunctionName = { fg = scheme.Blue },
        cssPseudoClassId = { fg = scheme.Slate_Grey },
        cssSelectorOp = { fg = scheme.Purple },
        cssSelectorOp2 = { fg = scheme.Purple },
        cssStringQ = { fg = scheme.Green },
        cssStringQQ = { fg = scheme.Green },
        cssTagName = { fg = scheme.Magenta },
        cssAttr = {},
        --]]
}
    return syntax
end

function setup.colors_terminal()
    g.terminal_color_0 = scheme.Black
    g.terminal_color_1 = scheme.Red
    g.terminal_color_2 = scheme.Green
    g.terminal_color_3 = scheme.Orange
    g.terminal_color_4 = scheme.Blue
    g.terminal_color_5 = scheme.Magenta
    g.terminal_color_6 = scheme.Blue
    g.terminal_color_7 = scheme.Grey
    g.terminal_color_8 = scheme.Slate_Grey
    g.terminal_color_9 = scheme.Red
    g.terminal_color_10 = scheme.Green
    g.terminal_color_11 = scheme.Orange
    g.terminal_color_12 = scheme.Mauve
    g.terminal_color_13 = scheme.Pink
    g.terminal_color_14 = scheme.Blue
    g.terminal_color_15 = scheme.Grey
end

return setup
